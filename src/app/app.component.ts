import { Component, OnInit } from '@angular/core';
import { DatasvcService } from './datasvc.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  form: any;
  submitted = false;
  id: any;
  projects: any;
  public constructor(public rest: DatasvcService, private formBuilder: FormBuilder) {
    this.getData();
  }
  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        title: ['', Validators.required],
        description: ['', Validators.required],

      }

    );
  }

  getData() {
    this.rest.getData().subscribe((data) => {
      if (data) {
        this.projects = data

      }
    })
  }


  edit(data: any) {
    this.form.controls['title'].setValue(data.title);
    this.form.controls['description'].setValue(data.description);
    this.id = data.id;

  }
  delete(data: any) {

    this.rest.deleteProject(data.id).subscribe(data => {
      if (data) {
        this.getData();
      }
    })
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    if (this.id) {
      this.rest.editProject(this.id, (this.form.value)).subscribe(data => {
        if (data) {
          this.getData();
        }
      })
    } else {
      this.rest.addProject(JSON.stringify(this.form.value)).subscribe(data => {
        if (data) {
          this.getData();
        }
      })
    }


  }
  
  serach() {

  }


}
