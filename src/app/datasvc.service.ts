import { Injectable } from '@angular/core';
 import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DatasvcService {

 constructor(private http: HttpClient) { }
  getData() {
    let url = 'https://fakestoreapi.com/products';
    return this.http.get(url);
  }

  addProject(body:any){
    let url = 'https://fakestoreapi.com/products';
    return this.http.post(url, body);
  }

  deleteProject(id:any){
    let url = 'https://fakestoreapi.com/products/'+id;
    return this.http.delete(url);
  }

  editProject(id:any,body:any){
    let url = 'https://fakestoreapi.com/products/'+id;
    return this.http.put(url, body);
  }
}
